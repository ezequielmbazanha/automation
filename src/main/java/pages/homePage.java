package pages;

import bsh.Variable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class homePage extends basePage{

    //Constructor
    public homePage(WebDriver driver){
        super(driver);
    }

    //Variables
    //String baseURL = "https://www.google.com";
    String baseURL = "http://automationpractice.com";

    //Locators
    By logInLink = By.xpath("//a[contains(text(),'Sign in')]");
    By pageLogo = By.xpath("//img[@alt='My Store']");

    //Methods
    public homePage goToHomePage(){
        driver.get(baseURL);
        return this;
    }

    public loginPage goToLoginPage() throws InterruptedException {
        click(logInLink);
        return new loginPage(driver);
    }
}
